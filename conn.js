var mysql = require('mysql2');
var dotenv = require('dotenv');
dotenv.config();
var conn = mysql.createConnection({
    host:"127.0.0.1",
    port: "3306",
    user: "student",
    password: "12345678",
    database: "studentDb",
    socketPath: '/cloudsql/nodeapp-374815:asia-south2:sks',
    connectionLimit: 10
});
conn.connect(function (err) {
    if (err) {
        // throw err;
        console.log(err);
    }
    else {
        console.log('Connection Established!!');
    }
});
module.exports = conn;
